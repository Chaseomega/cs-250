#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	int cycles = 0;

	ofstream output(logFile);

	output << "First Come First Served (FCFS)" << endl;

	while (jobQueue.Size() != 0)
	{
		output <<"ID:" << jobQueue.Front()->id << "   " <<  left << "CYCLE" << setw(10) << cycles  
			   << setw(15) << "REMAINING:"  << jobQueue.Front()->fcfs_timeRemaining 
			   << endl;

		jobQueue.Front()->Work(FCFS);

		if (jobQueue.Front()->fcfs_done)
		{
			output << "Done" << endl << endl << "--------------------------------------------" << endl;
			jobQueue.Front()->SetFinishTime(cycles ,FCFS);
			jobQueue.Pop();			
		}

		cycles++;
	}
	int total = 0;

	output << "First come, First Serve results:" << endl 
		   << setw(10) << "JOB ID" << "TIME TO COMPLETE" << endl;

	for (int i = 0; i < allJobs.size(); i++)
	{
		output << setw(10) << allJobs[i].id  << allJobs[i].fcfs_finishTime << endl;
		total = allJobs[i].fcfs_finishTime + total;
	}

	output << endl << "Total time: " << cycles << endl
		   << "(Time for all jobs to complete processing)"
		   << endl << endl;

	output << "Average time: " << (double)total / allJobs.size() << endl 
		   << "(Average time to complete, including the wait time while items " 
		   << endl << "are ahead of it in the queue.)";

	output.close();
	
}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	int cycles = 0;
	int timer = 0;
	ofstream output(logFile);

	output << "Round Robin (RR)" << endl;

	while (jobQueue.Size() != 0)
	{
		if (timer == timePerProcess) 
		{
			jobQueue.Front()->rr_timesInterrupted++;
			jobQueue.Push(jobQueue.Front());
			jobQueue.Pop();
			timer = 0;
			output << "--------------------------------------------" << endl;
		}

		output << "ID:" << jobQueue.Front()->id << "   " << left << "CYCLE" << setw(10) << cycles
			   << setw(15) << "REMAINING:" << jobQueue.Front()->rr_timeRemaining
		       << endl;

		jobQueue.Front()->Work(RR);

		if (jobQueue.Front()->rr_done)
		{
			output << "Done" << endl << endl << "--------------------------------------------" << endl;
			jobQueue.Front()->SetFinishTime(cycles, RR);
			jobQueue.Pop();
		}

		cycles++;
		timer++;
	}

	int total = 0;

	output << "Round Robin results:" << endl
		   << setw(10)<< "JOB ID" << setw(20) << "TIME TO COMPLETE" << "TIMES INTERRUPTED" << endl;

	for (int i = 0; i < allJobs.size(); i++)
	{
		output << left << setw(10) << allJobs[i].id << setw(20) << allJobs[i].rr_finishTime
			   << allJobs[i].rr_timesInterrupted << endl;

		total = allJobs[i].rr_finishTime + total;
	}

	output << endl << "Total time: " << cycles << endl
		   << "(Time for all jobs to complete processing)"
	       << endl << endl;

	output << "Average time: " << (double)total / allJobs.size()
		   << endl << "(Average time to complete, including the wait time while items "
		   << endl << "are ahead of it in the queue.)" << endl << endl;

	output << "Round Robin interval: " << timePerProcess << endl
		   << "Every n units of time, move the current item being processed" << endl
		   << "to the back of the queue and start working on the next item" << endl;

	output.close();
}

#endif
