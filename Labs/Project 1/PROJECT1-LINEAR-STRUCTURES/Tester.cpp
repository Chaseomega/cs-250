#include "Tester.hpp"
#include "string"
/*
This is the testers we are working on for project 1,
but there have been modifications for the following:

* Functions that used to return true on success and false on fail
have been updated to use exceptions, so they have no return type.
Therefore, these tests aren't needed and were removed.

* With a linked list there is no "max value" so tests for when the
list was full was removed.
*/

void Tester::RunTests()
{
	Test_IsEmpty();
	Test_IsFull();
	Test_Size();
	Test_GetCountOf();
	Test_Contains();

	Test_PushFront(); //Will need to implement function before this doesn't spit out exception.
	Test_PushBack();

	Test_Get();
	Test_GetFront();
	Test_GetBack();

	Test_PopFront();
	Test_PopBack(); //Will need to implement function before this doesn't spit out exception.
	// Test_PushBack(); Duplicate tester. Will comment out in case there is some other reson to have it in here.
	Test_Clear();

	Test_ShiftRight();
	Test_ShiftLeft();

	Test_Remove();
	Test_Insert(); //Will need to implement function before this doesn't spit out exception.
}

void Tester::DrawLine()
{
	cout << endl;
	for (int i = 0; i < 80; i++)
	{
		cout << "-";
	}
	cout << endl;
}

void Tester::Test_Init()
{
	DrawLine();
	cout << "TEST: Test_Init" << endl;

	{
		cout << endl << "Test 1: Create list and determine size is zero." << endl;
		List<string> testList;
		int expectedOutput = 0;
		int actualOutput = testList.Size();

		cout << "Expected output at 0: " << expectedOutput << endl;
		cout << "Actual output at 0: " << actualOutput << endl << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}


	// Put tests here
}

void Tester::Test_ShiftRight()
{
	DrawLine();
	cout << "TEST: Test_ShiftRight" << endl;

	// Put tests here
	{
		cout << endl << "Test 1" << endl;
		List<string> testList;
		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");

		testList.ShiftRight(1);
		string expectedOutputIndex0 = "A";
		string expectedOutputIndex2 = "B";
		string expectedOutputIndex3 = "C";
		string actualOutputIndex0;
		string actualOutputIndex2;
		string actualOutputIndex3;

		if (testList.Get(3) == nullptr)
		{
			cout << "Not the segfaults, not the bees... I mean segfaults";
			actualOutputIndex0 = "SegCheck";
			actualOutputIndex2 = "SegCheck";
			actualOutputIndex3 = "SegCheck";
		}
		else
		{
			actualOutputIndex0 = *testList.Get(0);
			actualOutputIndex2 = *testList.Get(2);
			actualOutputIndex3 = *testList.Get(3);
		}

		cout << "Expected output at 0: " << expectedOutputIndex0 << endl;
		cout << "Actual output at 0: " << actualOutputIndex0 << endl << endl;
		cout << "Expected output at 2: " << expectedOutputIndex2 << endl;
		cout << "Actual output at 2: " << actualOutputIndex2 << endl << endl;
		cout << "Expected output at 3: " << expectedOutputIndex3 << endl;
		cout << "Actual output at 3: " << actualOutputIndex3 << endl;

		if (expectedOutputIndex0 == actualOutputIndex0 && expectedOutputIndex2 == actualOutputIndex2 && expectedOutputIndex3 == actualOutputIndex3)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

	{
		cout << endl << "Test 2" << endl << endl;
		List<string> testList;
		for (int i = 0; i < 30; i++)
		{
			testList.PushBack("Z");
		}
		testList.ShiftRight(0);

		int expectedOutput = 31;
		int actualOutput = testList.Size();

		cout << "Expected output for size:  " << expectedOutput << endl;
		cout << "Actual output for size: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

}

void Tester::Test_ShiftLeft()
{
	DrawLine();
	cout << "TEST: Test_ShiftLeft" << endl;

	{
		cout << endl << "Test 1" << endl << endl;
		List<string> testList;
		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");

		testList.ShiftLeft(1);
		string expectedOutputIndex0 = "A";
		string expectedOutputIndex1 = "C";
		string actualOutputIndex0;
		string actualOutputIndex1;

		if (testList.Get(2) == nullptr)
		{
			cout << "Not the segfaults, not the bees... I mean segfaults";
			actualOutputIndex0 = "SegCheck";
			actualOutputIndex1 = "SegCheck";
		}
		else
		{
			actualOutputIndex0 = *testList.Get(0);
			actualOutputIndex1 = *testList.Get(2);
		}

		cout << "Expected output at 0: " << expectedOutputIndex0 << endl;
		cout << "Actual output at 0: " << actualOutputIndex0 << endl << endl;
		cout << "Expected output at 1 " << expectedOutputIndex1 << endl;
		cout << "Actual output at 1: " << actualOutputIndex1 << endl;

		if (expectedOutputIndex0 == actualOutputIndex0 && expectedOutputIndex1 == actualOutputIndex1)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

	{
		cout << endl << "Test 2" << endl << endl;
		List<string> testList;
		for (int i = 0; i < 30; i++)
		{
			testList.PushBack("Z");
		}
		testList.ShiftLeft(0);

		int expectedOutput = 30;
		int actualOutput = testList.Size();

		cout << "Expected output for size:  " << expectedOutput << endl;
		cout << "Actual output for first position: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	// Put tests here
}

void Tester::Test_Size()
{
	DrawLine();
	cout << "TEST: Test_Size" << endl << endl;

	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;
		int expectedSize = 0;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2" << endl;
		List<int> testList;

		testList.PushBack(1);

		int expectedSize = 1;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_IsEmpty()
{
	DrawLine();
	cout << "TEST: Test_IsEmpty" << endl;

	// Put tests here
	{
		// Test 1
		cout << endl << "Test 1" << endl;
		List<int> listA;
		bool expectedValue = true;
		bool actualValue = listA.IsEmpty();

		cout << "Created list, didn't add anything, should be empty..." << endl;
		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}

	{
		// Test 2
		cout << endl << "Test 1" << endl;
		List<int> listA;
		listA.PushBack(5);
		bool expectedValue = false;
		bool actualValue = listA.IsEmpty();

		cout << "Created list, added one thing, shouldn't be empty..." << endl;
		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}
}

void Tester::Test_IsFull()
{
	DrawLine();
	cout << "TEST: Test_IsFull" << endl;
	{
		cout << endl << "Test 1: Add one value to array, check if full.." << endl << endl;
		List<string> testList;

		testList.PushBack("A");

		bool expectedOutput = false;
		bool actualOutput = testList.IsFull();

		cout << "Expected output:  " << boolalpha << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Test 2: Fill array and check if full, should return true." << endl << endl;
		List<string> testList;

		for (int i = 0; i < 100; i++)
		{
			testList.PushBack("Z");
		}

		bool expectedOutput = true;
		bool actualOutput = testList.IsFull();

		cout << "Expected output:  " << boolalpha << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Test 3: Fill array and then add extra check if full, should return true." << endl << endl;
		List<string> testList;

		for (int i = 0; i < 105; i++)
		{
			testList.PushBack("Z");
		}

		bool expectedOutput = true;
		bool actualOutput = testList.IsFull();

		cout << "Expected output:  " << boolalpha << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Test 4: Create empty array and check if full, should return false." << endl << endl;
		List<string> testList;

		bool expectedOutput = false;
		bool actualOutput = testList.IsFull();

		cout << "Expected output:  " << boolalpha << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	// Put tests here
}

void Tester::Test_PushFront()
{
	DrawLine();
	cout << "TEST: Test_PushFront" << endl;

	// Put tests here
	{
		{
			cout << endl << "Test 1: Createlist, PushFront A" << endl << endl;
			List<string> testList;

			bool pushFrontExpectedReturn = true;
			bool didPushFrontWork = testList.PushFront("A");
			string expectedOutputIndex0 = "A";
			string ActualOutputIndex0;
			int expectedSize = 1;
			int actualSize = testList.Size();

			if (testList.Get(0) == nullptr)
			{
				cout << "Not the segfaults, not the bees... I mean segfaults";
				ActualOutputIndex0 = "SegCheck";
			}
			else
			{
				ActualOutputIndex0 = *testList.Get(0);
			}

			cout << "Expected output: " << boolalpha << pushFrontExpectedReturn << endl;
			cout << "Actual output: " << didPushFrontWork << endl << endl;
			cout << "Expected output: " << expectedOutputIndex0 << endl;
			cout << "Actual output: " << ActualOutputIndex0 << endl << endl;
			cout << "Expected size: " << expectedSize << endl;
			cout << "Actual size: " << actualSize << endl;

			if (pushFrontExpectedReturn == didPushFrontWork && expectedOutputIndex0 == ActualOutputIndex0 && expectedSize == actualSize)
			{
				cout << "Pass" << endl;
			}
			else
			{
				cout << "Fail" << endl;
			}

		}

		{
			cout << endl << "Test 2: Create PushFront multiple items " << endl << endl;
			List<string> testList;

			bool pushFrontExpectedReturn = true;
			bool didPushFrontWork1 = testList.PushFront("A");
			bool didPushFrontWork2 = testList.PushFront("B");
			bool didPushFrontWork3 = testList.PushFront("C");
			string expectedOutputIndex0 = "C";
			string expectedOutputIndex1 = "B";
			string expectedOutputIndex2 = "A";
			string actualOutputIndex0;
			string actualOutputIndex1;
			string actualOutputIndex2;
			int expectedSize = 3;
			int actualSize = testList.Size();

			if (testList.Get(2) == nullptr)
			{
				cout << "Not the segfaults, not the bees... I mean segfaults";
				actualOutputIndex0 = "would cause segfaults";
				actualOutputIndex1 = "would cause segfaults";
				actualOutputIndex2 = "would cause segfaults";
			}
			else
			{
				actualOutputIndex0 = *testList.Get(0);
				actualOutputIndex1 = *testList.Get(1);
				actualOutputIndex2 = *testList.Get(2);
			}

			cout << "Expected output: " << boolalpha << pushFrontExpectedReturn << endl;
			cout << "Actual output: " << didPushFrontWork1 << endl << endl;
			cout << "Expected output: " << pushFrontExpectedReturn << endl;
			cout << "Actual output: " << didPushFrontWork2 << endl << endl;
			cout << "Expected output: " << pushFrontExpectedReturn << endl;
			cout << "Actual output: " << didPushFrontWork3 << endl << endl;
			cout << "Expected output: " << expectedOutputIndex0 << endl;
			cout << "Actual output: " << actualOutputIndex0 << endl << endl;
			cout << "Expected output: " << expectedOutputIndex1 << endl;
			cout << "Actual output: " << actualOutputIndex1 << endl << endl;
			cout << "Expected output: " << expectedOutputIndex2 << endl;
			cout << "Actual output: " << actualOutputIndex2 << endl << endl;
			cout << "Expected size: " << expectedSize << endl;
			cout << "Actual size: " << actualSize << endl;

			if (pushFrontExpectedReturn == didPushFrontWork1 && pushFrontExpectedReturn == didPushFrontWork2 &&
				pushFrontExpectedReturn == didPushFrontWork3 && expectedOutputIndex0 == actualOutputIndex0 &&
				expectedOutputIndex1 == actualOutputIndex1 && expectedOutputIndex2 == actualOutputIndex2 &&
				expectedSize == actualSize)
			{
				cout << "Pass" << endl;
			}
			else
			{
				cout << "Fail" << endl;
			}

		}

		{
			cout << endl << "Test 3: Create list, PushFront 100 items. Try one more" << endl << endl;
			List<string> testList;

			for (int i = 0; i < 100; i++)
			{
				testList.PushBack("Z");
			}

			bool pushFrontExpectedReturn = false;
			bool didPushFrontWork = testList.PushFront("A");
			int expectedSize = 100;
			int actualSize = testList.Size();

			cout << "Expected output: " << boolalpha << pushFrontExpectedReturn << endl;
			cout << "Actual output: " << didPushFrontWork << endl << endl;
			cout << "Expected size: " << expectedSize << endl;
			cout << "Actual size: " << actualSize << endl;

			if (pushFrontExpectedReturn == didPushFrontWork && expectedSize == actualSize)
			{
				cout << "Pass" << endl;
			}
			else
			{
				cout << "Fail" << endl;
			}

		}
	}

}

void Tester::Test_PushBack()
{
	DrawLine();
	cout << "TEST: Test_PushBack" << endl;
	{
		cout << endl << "Test 1: Create a list, input 'A'." << endl;

		List<string> testlist;

		// Things to test

		bool expectedReturn = true;
		bool actualReturn = testlist.PushBack("A");

		int expectedSize = 1;
		int actualSize = testlist.Size();

		// Remember that Get functions return a pointer in this project.
		string expectedValueAt0 = "A";
		string* actualValueAt0 = testlist.Get(0);

		if (actualValueAt0 == nullptr)
		{
			cout << "nullptr - exiting test" << endl;
			return;
		}

		// Display checks
		cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;
		cout << "Size, expected: " << expectedSize << ", actual: " << actualSize << endl;
		cout << "Value, expected: " << expectedValueAt0 << ", actual: " << *actualValueAt0 << endl;

		// Check each value
		if (expectedReturn != actualReturn)
		{
			cout << "FAIL - Bad return" << endl;
		}
		else if (expectedSize != actualSize)
		{
			cout << "FAIL - Bad size" << endl;
		}
		else if (expectedValueAt0 != *actualValueAt0)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}

	{
		cout << endl << "Test 2: Push Back 'A', 'B', 'C', make sure 0 = A, 1 = B, 2 = C" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");

		string expectedValue_0 = "A";
		string expectedValue_1 = "B";
		string expectedValue_2 = "C";

		string* actualValue_0 = testlist.Get(0);
		string* actualValue_1 = testlist.Get(1);
		string* actualValue_2 = testlist.Get(2);

		if (actualValue_0 == nullptr || actualValue_1 == nullptr || actualValue_2 == nullptr)
		{
			cout << "nullptrs returned; exiting to avoid segfault" << endl;
			return;
		}

		cout << "Expected value at 0: " << expectedValue_0 << endl;
		cout << "Expected value at 1: " << expectedValue_1 << endl;
		cout << "Expected value at 2: " << expectedValue_2 << endl;

		cout << "Actual value at 0: " << *actualValue_0 << endl;
		cout << "Actual value at 1: " << *actualValue_1 << endl;
		cout << "Actual value at 2: " << *actualValue_2 << endl;

		if (expectedValue_0 != *actualValue_0 ||
			expectedValue_1 != *actualValue_1 ||
			expectedValue_2 != *actualValue_2)
		{
			cout << "FAIL - Bad value(s)" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}

	{
		cout << endl << "Test 3: Create a list, insert 101 items, Size() should return 100" << endl;

		List<string> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack("Z");
		}

		bool expectedReturn = false;
		bool actualReturn = testlist.PushBack("A");

		int expectedValue = 100;
		int actualValue = testlist.Size();

		cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;
		cout << "Size, expected: " << expectedValue << ", actual: " << actualValue << endl;

		if (expectedValue != actualValue)
		{
			cout << "FAIL - Bad size" << endl;
		}
		else if (expectedReturn != actualReturn)
		{
			cout << "FAIL - Bad return" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
}

void Tester::Test_PopFront()
{
	DrawLine();
	cout << "TEST: Test_PopFront" << endl;

	// Put tests here
	{
		cout << endl << "Test 1: Create an empty list. Pop should return false." << endl;

		List<int> testlist;

		bool expectedReturn = false;
		bool actualReturn = testlist.PopFront();

		cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;

		if (expectedReturn != actualReturn)
		{
			cout << "FAIL - Bad return" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}

	{
		cout << endl << "Test 2: PushBack 'A', 'B', 'C', make sure PopFront removes correct item." << endl;

		List<string> testlist;

		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");

		bool expectedReturn = true;
		bool actualReturn = testlist.PopFront();

		string expected0 = "B";
		string* actual0 = testlist.Get(0);

		if (actual0 == nullptr)
		{
			cout << "nullptr" << endl;
			return;
		}

		cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;
		cout << "Item at 0, expected: " << expected0 << ", actual: " << *actual0 << endl;

		if (expectedReturn != actualReturn)
		{
			cout << "FAIL - Bad return" << endl;
		}
		else if (expected0 != *actual0)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
}

void Tester::Test_PopBack()
{
	DrawLine();
	cout << "TEST: Test_PopBack" << endl;


	{
		cout << endl << "Test 1: Create a list, try PopBack." << endl << endl;
		List<string> testList;

		bool expectedOutput = false;
		bool actualOutput = testList.PopBack();


		cout << "Expected output:  " << boolalpha << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Test 2: Create a list with three items, try PopBack." << endl << endl;
		List<string> testList;
		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");

		bool popBackExpectedReturn = true;
		bool didPopFrontWork = testList.PopBack();
		string expectedOutputBack = "B";
		string actualOutputBack;

		if (testList.GetBack() == nullptr)
		{
			actualOutputBack = "SegCheck";
		}

		else
		{
			actualOutputBack = *testList.GetBack();
		}


		cout << "Expected output:  " << boolalpha << popBackExpectedReturn << endl;
		cout << "Actual output: " << didPopFrontWork << endl << endl;
		cout << "Expected Output: " << expectedOutputBack << endl;
		cout << "Actual Output: " << actualOutputBack << endl;


		if (popBackExpectedReturn == didPopFrontWork && expectedOutputBack == actualOutputBack)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	// Put tests here

}

void Tester::Test_Clear()
{
	DrawLine();
	cout << "TEST: Test_Clear" << endl;
	{
		cout << endl << "Test 1" << endl << endl;
		List<string> testList;
		for (int i = 0; i < 100; i++)
		{
			testList.PushBack("Z");
		}
		testList.Clear();

		int expectedOutput = 0;
		int actualOutput = testList.Size();

		cout << "Expected output for size:  " << expectedOutput << endl;
		cout << "Actual output for first position: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Test 2" << endl << endl;
		List<string> testList;

		testList.Clear();

		int expectedOutput = 0;
		int actualOutput = testList.Size();

		cout << "Expected output for size:  " << expectedOutput << endl;
		cout << "Actual output for first position: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Test 3" << endl << endl;
		List<string> testList;
		for (int i = 0; i < 30; i++)
		{
			testList.PushBack("Z");
		}
		testList.Clear();

		int expectedOutput = 0;
		int actualOutput = testList.Size();

		cout << "Expected output for size:  " << expectedOutput << endl;
		cout << "Actual output for first position: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	// Put tests here
}

void Tester::Test_Get()
{
	DrawLine();
	cout << "TEST: Test_Get" << endl;
	
	{
		cout << endl << "Test 1: Create a list, try Get(0)." << endl << endl;
		List<string> testList;

		string* expectedOutput = nullptr;
		string* actualOutput = testList.Get(0);


		cout << "Expected output:  " << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Test 2: Create a list with three items, try PopBack." << endl << endl;
		List<string> testList;
		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");

		string expectedOutputIndex0 = "A";
		string expectedOutputIndex1 = "B";
		string expectedOutputIndex2 = "C";
		string actualOutputIndex0;
		string actualOutputIndex1;
		string actualOutputIndex2;

		if (testList.Get(2) == nullptr)
		{
			cout << "No segfaults plox";
			actualOutputIndex0 = "SegCheck";
			actualOutputIndex1 = "SegCheck";
			actualOutputIndex2 = "SegCheck";
		}
		else
		{
			
			actualOutputIndex0 = *testList.Get(0);
			actualOutputIndex1 = *testList.Get(1);
			actualOutputIndex2 = *testList.Get(2);
		}
		


		cout << "Expected output:  " << expectedOutputIndex0 << endl;
		cout << "Actual output: " << actualOutputIndex0 << endl << endl;
		cout << "Expected Output: " << expectedOutputIndex1 << endl;
		cout << "Actual Output: " << actualOutputIndex1 << endl << endl;
		cout << "Expected Output: " << expectedOutputIndex2 << endl;
		cout << "Actual Output: " << actualOutputIndex2 << endl;


		if (expectedOutputIndex0 == actualOutputIndex0 && 
			expectedOutputIndex1 == actualOutputIndex1 && 
			expectedOutputIndex2 == actualOutputIndex2)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	// Put tests here
}

void Tester::Test_GetFront()
{
	DrawLine();
	cout << "TEST: Test_GetFront" << endl;

	// Put tests here
	{
		cout << endl << "Test 1: GetFront() from empty list should return nullptr." << endl;

		List<string> testlist;

		string* expectedAddress = nullptr;
		string* actualAddress = testlist.GetFront();

		cout << "Address, expected: " << expectedAddress << ", actual: " << actualAddress << endl;

		if (expectedAddress != actualAddress)
		{
			cout << "FAIL - Bad address" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}

	{
		cout << endl << "Test 2: Push Back 'A', 'B', 'C', GetFront() should return A" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");

		string expectedValue = "A";
		string* actualValue = testlist.GetFront();

		if (actualValue == nullptr)
		{
			cout << "Nullptr" << endl;
			return;
		}

		cout << "Value, expected: " << expectedValue << ", actual: " << *actualValue << endl;

		if (*actualValue != expectedValue)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
}

void Tester::Test_GetBack()
{
	DrawLine();
	cout << "TEST: Test_GetBack" << endl;

	{
		cout << endl << "Test 2: Create empty list, attempt GetBack. Should return nullptr " << endl << endl;
		List<string> testList;

		string* expectedOutput = nullptr;
		string* actualOutput = testList.GetBack();

		cout << "Expected output:  " << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

	{
		cout << endl << "Test 2: Create list with three entries. Should return last entry " << endl << endl;
		List<string> testList;
		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");

		string expectedOutput = "C";
		string actualOutput = *testList.GetBack();

		if (testList.GetBack() == nullptr)
		{
			cout << "No segfaults plox";
			actualOutput = "SegCheck";
		}
		else
		{

			actualOutput = *testList.GetBack();
		}


		cout << "Expected output:  " << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	// Put tests here
}

void Tester::Test_GetCountOf()
{
	DrawLine();
	cout << "TEST: Test_GetCountOf" << endl;
	{
		cout << endl << "Test 1: Create a list, try GetCountOf(""A"")." << endl << endl;
		List<string> testList;

		int expectedOutput = 0;
		int actualOutput = testList.GetCountOf("A");


		cout << "Expected output:  " << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Test 2: Create a list with three items, try GetCountOf(""A"")" << endl << endl;
		List<string> testList;
		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");

		int expectedOutput = 1;
		int actualOutput = testList.GetCountOf("A");


		cout << "Expected output:  " << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;


		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Test 2: Create a list with three items, try PopBack." << endl << endl;
		List<string> testList;
		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");
		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");

		int expectedCountOfA = 2;
		int actualCountOfA = testList.GetCountOf("A");
		int expectedCountOfZ = 0;
		int actualCountOfZ = testList.GetCountOf("Z");


		cout << "Expected output:  " << expectedCountOfA << endl;
		cout << "Actual output: " << actualCountOfA << endl;
		cout << "Expected output:  " << expectedCountOfZ << endl;
		cout << "Actual output: " << actualCountOfZ << endl;


		if (expectedCountOfA == actualCountOfA &&
			expectedCountOfZ == actualCountOfZ)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	// Put tests here
}

void Tester::Test_Contains()
{
	DrawLine();
	cout << "TEST: Test_Contains" << endl;

	{
		cout << endl << "Test 1: Test function on empty list." << endl << endl;
		List<string> testList;

		bool expectedOutput = false;
		bool actualOutput = testList.Contains("A");

		cout << "Expected output:  " << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Test 2: Fill array and check for value that list does contain, should return true." << endl << endl;
		List<string> testList;

		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");

		bool expectedOutput = true;
		bool actualOutput = testList.Contains("C");

		cout << "Expected output:  " << boolalpha << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Test 3: Fill array and check for value that list doesn't contain, should return false." << endl << endl;
		List<string> testList;

		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");

		bool expectedOutput = false;
		bool actualOutput = testList.Contains("D");

		cout << "Expected output:  " << boolalpha << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	// Put tests here
}

void Tester::Test_Remove()
{
	DrawLine();
	cout << "TEST: Test_Remove" << endl;

	{
		cout << endl << "Index Test 1: Fill array and remove one entry from 0 index." << endl << endl;
		List<string> testList;

		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");

		bool expectedRemoveWorkReturn = true;
		bool didRemoveWork = testList.RemoveIndex(0);
		string expectedReturnIndex0 = "B";
		string actualReturnIndex0;

		if (testList.Get(0) == nullptr)
		{
			actualReturnIndex0 = "SegCheck";
		}

		else
		{
			actualReturnIndex0 = *testList.Get(0);
		}

		cout << "Expected output:  " << boolalpha << expectedRemoveWorkReturn << endl;
		cout << "Actual output: " << didRemoveWork << endl;
		cout << "Expected output at 0:  " << expectedReturnIndex0 << endl;
		cout << "Actual output at 0: " << actualReturnIndex0 << endl;

		if (expectedRemoveWorkReturn == didRemoveWork && expectedReturnIndex0 == actualReturnIndex0)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Index Test 2: Remove from empty list, should fail when index is out of list range." << endl << endl;
		List<string> testList;

		bool expectedOutput = false;
		bool actualOutput = testList.RemoveIndex(1);

		cout << "Expected output:  " << boolalpha << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;

		if (expectedOutput == actualOutput)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		cout << endl << "Index Test 3: Remove from index within bounds but non zero, should return true " << endl << endl;
		List<string> testList;

		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");
		testList.PushBack("D");

		bool expectedRemoveWorkReturn = true;
		bool didRemoveWork = testList.RemoveIndex(2);
		string expectedReturnIndex2 = "D";
		string actualReturnIndex2;
		if (testList.Get(2) == nullptr)
		{
			actualReturnIndex2 = "SegCheck";
		}
		
		else
		{
			actualReturnIndex2 = *testList.Get(2);
		}

		cout << "Expected output:  " << boolalpha << expectedRemoveWorkReturn << endl;
		cout << "Actual output: " << didRemoveWork << endl;
		cout << "Expected output at 2:  " << expectedReturnIndex2 << endl;
		cout << "Actual output at 2: " << actualReturnIndex2 << endl;

		if (expectedRemoveWorkReturn == didRemoveWork && expectedReturnIndex2 == actualReturnIndex2)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

	{
		cout << endl << "Item Test 1: Remove an item from list with multiple entries of said item " << endl << endl;
		List<string> testList;

		testList.PushBack("A");
		testList.PushBack("A");
		testList.PushBack("A");
		testList.PushBack("C");

		bool expectedRemoveWorkReturn = true;
		bool didRemoveWork = testList.RemoveItem("A");
		testList.RemoveItem("A");
		int expectedSize = 1;
		int actualSize = testList.Size();
		string expectedOutputIndex0 = "C";
		string actualOutputIndex0;

		if (testList.Get(0) == nullptr)
		{
			actualOutputIndex0 = "SegCheck";
		}

		else
		{
			actualOutputIndex0 = *testList.Get(0);
		}

		cout << "Expected output:  " << boolalpha << expectedRemoveWorkReturn << endl;
		cout << "Actual output: " << didRemoveWork << endl;
		cout << "Expected size:  " << expectedSize << endl;
		cout << "Actual size: " << actualSize << endl;
		cout << "Expected output at 0:  " << expectedOutputIndex0 << endl;
		cout << "Actual output at 0: " << actualOutputIndex0 << endl;

		if (expectedRemoveWorkReturn == didRemoveWork && expectedSize == actualSize && expectedOutputIndex0 == actualOutputIndex0)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

	{
		cout << endl << "Item Test 2: Remove an item not within the list, should return false " << endl << endl;
		List<string> testList;

		testList.PushBack("A");
		testList.PushBack("B");
		testList.PushBack("C");

		bool expectedRemoveWorkReturn = false;
		bool didRemoveWork = testList.RemoveItem("Z");
		int expectedSize = 3;
		int actualSize = testList.Size();

		cout << "Expected output:  " << boolalpha << expectedRemoveWorkReturn << endl;
		cout << "Actual output: " << didRemoveWork << endl;
		cout << "Expected size:  " << expectedSize << endl;
		cout << "Actual size: " << actualSize << endl;

		if (expectedRemoveWorkReturn == didRemoveWork && expectedSize == actualSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

	{
		cout << endl << "Item Test 3: Remove from empty list, should return false " << endl << endl;
		List<string> testList;


		bool expectedRemoveWorkReturn = false;
		bool didRemoveWork = testList.RemoveItem("A");
		int expectedSize = 0;
		int actualSize = testList.Size();

		cout << "Expected output:  " << boolalpha << expectedRemoveWorkReturn << endl;
		cout << "Actual output: " << didRemoveWork << endl;
		cout << "Expected size:  " << expectedSize << endl;
		cout << "Actual size: " << actualSize << endl;

		if (expectedRemoveWorkReturn == didRemoveWork && expectedSize == actualSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}
	// Put tests here
}

void Tester::Test_Insert()
{
	DrawLine();
	cout << "TEST: Test_Insert" << endl;

	{
		cout << endl << "Test 1: Create empty list, insert A at index 0 " << endl << endl;
		List<string> testList;



		bool expectedInsertWorkReturn = true;
		bool didInsertWork = testList.Insert(0, "A");
		int expectedsize = 1;
		int actualsize = testList.Size();

		cout << "Expected output:  " << boolalpha << expectedInsertWorkReturn << endl;
		cout << "Actual output: " << didInsertWork << endl;
		cout << "Expected size:  " << expectedsize << endl;
		cout << "Actual size: " << actualsize << endl;

		if (expectedInsertWorkReturn == didInsertWork && expectedsize == actualsize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

	{
		cout << endl << "Test 2: Create empty list, insert A at index 50 " << endl << endl;
		List<string> testList;



		bool expectedInsertWorkReturn = false;
		bool didInsertWork = testList.Insert(50, "A");
		int expectedsize = 0;
		int actualsize = testList.Size();

		cout << "Expected output:  " << boolalpha << expectedInsertWorkReturn << endl;
		cout << "Actual output: " << didInsertWork << endl;
		cout << "Expected size:  " << expectedsize << endl;
		cout << "Actual size: " << actualsize << endl;

		if (expectedInsertWorkReturn == didInsertWork && expectedsize == actualsize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

	{
		cout << endl << "Create empty list, insert A,B,C at index 0,1,2 respectively " << endl << endl;
		List<string> testList;



		bool expectedInsertWorkReturn = true;
		bool didInsertWork1 = testList.Insert(0, "A");
		bool didInsertWork2 = testList.Insert(1, "B");
		bool didInsertWork3 = testList.Insert(2, "C");
		string expectedOutputIndex0 = "A";
		string expectedOutputIndex1 = "B";
		string expectedOutputIndex2 = "C";
		string actualOutputIndex0;
		string actualOutputIndex1;
		string actualOutputIndex2;
		int expectedSize = 3;
		int actualSize = testList.Size();

		if (testList.Get(2) == nullptr)
		{
			cout << "Not the segFaults";
			actualOutputIndex0 = "segCheck";
			actualOutputIndex1 = "segCheck";
			actualOutputIndex2 = "segCheck";
		}

		else
		{
			actualOutputIndex0 = *testList.Get(0);
			actualOutputIndex1 = *testList.Get(1);
			actualOutputIndex2 = *testList.Get(2);
		}
		cout << "Expected output:  " << boolalpha << expectedInsertWorkReturn << endl;
		cout << "Actual output: " << didInsertWork1 << endl << endl;
		cout << "Expected output:  " << expectedInsertWorkReturn << endl;
		cout << "Actual output: " << didInsertWork2 << endl << endl;
		cout << "Expected output:  " << expectedInsertWorkReturn << endl;
		cout << "Actual output: " << didInsertWork3 << endl << endl;
		cout << "Expected output at 0:  " << expectedOutputIndex0 << endl;
		cout << "Actual output at 0: " << actualOutputIndex0 << endl << endl;
		cout << "Expected output at 1:  " << expectedOutputIndex1 << endl;
		cout << "Actual output at 1: " << actualOutputIndex1 << endl << endl;
		cout << "Expected output at 2:  " << expectedOutputIndex2 << endl;
		cout << "Actual output at 2: " << actualOutputIndex2 << endl << endl;
		cout << "Expected size:  " << expectedSize << endl;
		cout << "Actual size: " << actualSize << endl << endl;

		if (expectedInsertWorkReturn == didInsertWork1 && 
			expectedInsertWorkReturn == didInsertWork2 &&
			expectedInsertWorkReturn == didInsertWork3 && 
			expectedOutputIndex0 == actualOutputIndex0 &&
			expectedOutputIndex1 == actualOutputIndex1 && 
			expectedOutputIndex2 == actualOutputIndex2 &&
			expectedSize == actualSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}
}

// Put tests here
