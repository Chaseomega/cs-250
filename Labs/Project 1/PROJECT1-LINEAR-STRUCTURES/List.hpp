#ifndef _LIST_HPP
#define _LIST_HPP

#include <iostream>
using namespace std;

const int ARRAY_SIZE = 100;

template <typename T>
class List
{
private:
	// private member variables
	int m_itemCount = 0;
	T m_arr[ARRAY_SIZE];

	// functions for interal-workings
	bool ShiftRight(int atIndex)
	{
		if (atIndex < 0 || atIndex > m_itemCount)
		{
			return false;
		}

		m_itemCount++;

		for (int i = m_itemCount; i > atIndex; i--)
		{
			m_arr[i] = m_arr[i - 1];
		}

	}

	bool ShiftLeft(int atIndex)
	{
		if (atIndex < 0 || atIndex > m_itemCount)
		{
			return false;
		}

		for (int i = atIndex + 1; i < m_itemCount; i++)
		{
			m_arr[i - 1] = m_arr[i];
		}

		return true;
	}

public:
	List()
	{
		m_itemCount = 0;
	}

	~List()
	{
	}

	// Core functionality
	int     Size() const
	{
		return m_itemCount; // placeholder
	}

	bool    IsEmpty() const
	{
		if (m_itemCount == 0)
		{
			return true; // placeholder
		}
		else
		{
			return false;
		}
		
	}

	bool    IsFull() const
	{
		if (m_itemCount == ARRAY_SIZE)
		{
			return true; // placeholder
		}
		
		return false;
		
	}

	bool    PushFront(const T& newItem)
	{
		return Insert(0, newItem);
	}

	bool    PushBack(const T& newItem)
	{
		if (IsFull()) 
		{ return false; }

		m_arr[m_itemCount] = newItem;
		m_itemCount++;
		return true;
	}

	bool    Insert(int atIndex, const T& item)
	{
		if (IsFull())
		{
			return false;
		}; // placeholder

		if (atIndex > m_itemCount)
		{
			return false;
		}

		for (int i = m_itemCount; i > atIndex; i--)
		{
			m_arr[i] = m_arr[i - 1];
		}
		m_arr[atIndex] = item;
		m_itemCount++;
		return true;
	}

	bool    PopFront()
	{
		if (m_itemCount == 0)
		{
			return false;
		}
		
		for (int i = 0; i < m_itemCount; i++)
		{
			m_arr[i] = m_arr[i + 1];
		}
		m_itemCount--;
		return true; // placeholder
	}

	bool    PopBack()
	{
		if (IsEmpty()) { return false; }

		// lazy deletion
		m_itemCount--;
		return true;
	}

	bool    RemoveItem(const T& item)
	{
		if (IsEmpty())
		{
			return false;
		}

		// One way you can do this
		int j = 0;
		int i = 0;
		// Keep track of indices to remove
		while (j < (Size()))
		{
			if (m_arr[j] == item)
			{
				RemoveIndex(j);
				i++;
				continue;
			}
			j++;
		}
		if (i != 0)
		{
			return true;
		}
		
		return false;
	}

	bool    RemoveIndex(int atIndex)
	{
		if (m_itemCount == 0)
		{
			return false;
		}
		for (int i = atIndex; i < m_itemCount; i++)
		{
			m_arr[i] = m_arr[i + 1];
		}
		m_itemCount--;
		return true;
		 // placeholder
	}

	void    Clear()
	{
		// lazy deletion
		m_itemCount = 0;
	}

	// Accessors
	T*      Get(int atIndex)
	{
		if (atIndex < 0 || atIndex >= m_itemCount)
		{
			return nullptr;
		}

		return &m_arr[atIndex];
	}

	T*      GetFront()
	{
		return Get(0); // placeholder
	}

	T*      GetBack()
	{
		if (m_itemCount == 0)
		{
			return nullptr;
		}
		return &m_arr[m_itemCount - 1]; // placeholder
	}

	// Additional functionality
	int     GetCountOf(const T& item) const
	{
		int counter = 0;
		int j = 0;
		while (j < (Size()))
		{
			if (m_arr[j] == item)
			{
				counter++;
			}
			j++;
		}; // placeholder

		return counter;
	}

	bool Contains(const T& item) const
	{
		return (GetCountOf(item) > 0);
	}

	// Helper function
	void Display()
	{
		cout << "\t List size: " << Size() << endl;
		for (int i = 0; i < Size(); i++)
		{
			T* item = Get(i);

			cout << "\t " << i << " = ";

			if (item == nullptr)
			{
				cout << "nullptr" << endl;
			}
			else
			{
				cout << *item << endl;
			}
		}
	}

	friend class Tester;
};


#endif