#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "DATA_STRUCTURES/Stack.hpp" // LinkedStack

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

void CourseCatalog::LoadCourses() // done
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses() noexcept
{
    Menu::Header( "VIEW COURSES" );

	cout << "#\t" << "CODE\t" << "TITLE" << endl << "\tPREREQS"
		 << endl << endl;
	Menu::DrawHorizontalBar(80);

    for (int i = 0; i < m_courses.Size(); i++)
    {
        cout << i << "\t" << m_courses[i].code << "\t" << m_courses[i].name
             << endl << "\t" << m_courses[i].prereq << endl << endl;
    }
    return;
}

Course CourseCatalog::FindCourse( const string& code )
{
    for (int i = 0; i < m_courses.Size(); i++)
    {
       if (code == m_courses[i].code)
        return m_courses[i];
    }
    throw CourseNotFound( "Not yet implemented!" );
}

void CourseCatalog::ViewPrereqs()
{
    Menu::Header( "GET PREREQS" );
    string input;
    Course current;
	int counter = 1;
    cout << "  Enter class code" << endl << endl << "  >>";
    cin >> input;

	try 
	{
		current = FindCourse(input);
	}
	catch (CourseNotFound)
	{
		cout << "Error! Unable to find course " << input << endl;
		return;
	}
    LinkedStack<Course> Prereq;
    Prereq.Push(current);

    while (current.prereq != "")
    {
		try
		{
			current = FindCourse(current.prereq);
		}
		catch (CourseNotFound)
		{
			break;
		}
		
		Prereq.Push(current);
    }

	cout << endl << "Courses to take: " << endl;

	while (Prereq.Size() != 0)
	{
		cout << counter << ".  " << Prereq.Top().code << " " << Prereq.Top().name << endl;
		Prereq.Pop();
		counter++;
	}

}

void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
