#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"

template <typename T>
class LinkedStack
{
public:
	LinkedStack()
	{
		m_ptrFirst = nullptr;
		m_ptrLast = nullptr;
		m_itemCount = 0;
	}

	void Push(const T& newData) noexcept
	{
		Node<T>* newNode = new Node<T>;
		newNode->data = newData;

		if (m_ptrLast == nullptr)
		{
			// Point the first & last ptrs to the new node
			m_ptrFirst = newNode;
			m_ptrLast = newNode;
		}

		else
		{
			// Point the last node's ptrNext to the new node
			m_ptrLast->ptrNext = newNode;
			newNode->ptrPrev = m_ptrLast;
			m_ptrLast = newNode;
		}

		m_itemCount++;

	}

	T& Top()
	{
		if (m_itemCount == 0)
		{
			throw CourseNotFound("wut");
		}

		return m_ptrLast->data;
		// placeholder
	}

	void Pop() noexcept
	{
		if (m_ptrLast == nullptr)
		{
			return;
		}

		else if (m_ptrFirst == m_ptrLast)
		{
			delete m_ptrFirst;
			m_ptrFirst = nullptr;
			m_ptrLast = nullptr;
		}

		else
		{
			Node<T>* ptrSecondtoLast = m_ptrLast->ptrPrev;
			delete m_ptrLast;
			m_ptrLast = ptrSecondtoLast;
			m_ptrLast->ptrNext = nullptr;
		}
	
		m_itemCount--;
	}

	int Size()
	{
		return this->m_itemCount;    // placeholder
	}

private:
	Node<T>* m_ptrFirst;
	Node<T>* m_ptrLast;
	int m_itemCount;
};

#endif
