// Lab - Standard Template Library - Part 3 - Queues
// CHASE, STUMP

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue <float> pendingTransactions;
	int choice;
	bool done = false;
	float input;

	while (!done)
	{
		cout << endl << "----------------------------" << endl
			 << "Transactions queued: " << pendingTransactions.size()
			 << endl << endl
			 << "1. Enqueue transaction\t"
			 << "2. Continue" << endl << endl
			 << ">> ";

		cin >> choice;
		cout << endl;

		if (choice == 1)
		{
			cout << "Enter amount (positive or negative) for next transaction: ";
			cin >> input;
			pendingTransactions.push(input);

		}
		else if (choice == 2)
		{
			float balance = 0;
			while (!pendingTransactions.empty())
			{
				cout << pendingTransactions.front() << " pushed to account" << endl;
				balance += pendingTransactions.front();
				pendingTransactions.pop();
			}
			cout << endl << "Final balance: $" << balance << endl << endl;
			done = true;
			
		}

		else 
		{
			cout << "Incorrect input.";
		}
			

	}
	cout << "Happy to have you banking with us";
    cin.ignore();
    cin.get();
    return 0;
}
