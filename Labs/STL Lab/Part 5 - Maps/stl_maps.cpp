// Lab - Standard Template Library - Part 5 - Maps
// CHASE, STUMP

#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
	bool done = false;
	char color;
	map <char, string> colors;
	colors['r'] = "FF0000";
	colors['g'] = "00FF00";
	colors['b'] = "0000FF";
	colors['c'] = "00FFFF";
	colors['m'] = "FF00FF";
	colors['y'] = "FFFF00";
	
	while (!done)
	{
		cout << "Enter a color letter, or 'q' to stop: ";
		cin >> color;
		if (color == 'q')
		{
			done = true;
		}
		else
		{
			cout << "Hex: " << colors[color] << endl;
		}
	}
	cout << "See ya later!";
    cin.ignore();
    cin.get();
    return 0;
}
