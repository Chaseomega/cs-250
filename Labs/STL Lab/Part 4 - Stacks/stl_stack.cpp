// Lab - Standard Template Library - Part 4 - Stacks
// CHASE, STUMP

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
	stack<string> word;
	string input;
	bool done = false;

	cout << "Enter the next letter of the word, or UNDO to undo, or DONE to stop" << endl;

	while (!done)
	{
		cout << ">> ";
		cin >> input;
		if (input == "DONE")
		{
			done = true;
		}
		else if (input == "UNDO")
		{
			if (word.size() == 0)
			{
				cout << "\tThere are no letters to undo" << endl;
			}
			else
			{
				cout << "\tRemoved " << word.top() << endl;
				word.pop();
			}
			
		}
		else
		{
			word.push(input);
		}
	}
	cout << endl << endl << "Finished word: ";
	while (!word.empty())
	{
		cout << word.top();
		word.pop();
	}
    cin.ignore();
    cin.get();
    return 0;
}
