// Lab - Standard Template Library - Part 2 - Lists
// CHASE, STUMP

#include <iostream>
#include <list>
#include <string>

using namespace std;
void  DisplayList(list <string >&  states);

int main()
{
	list <string> states;
	string userInput;
	bool finished = false;

	while (!finished)
	{
		cout << "----------------------------" << endl
			<< "Number of states logged: " << states.size()
			<< endl << endl
			<< "1. Add new state to front\t"
			<< "2. Add new state to back" << endl
			<< "3. Pop front state\t\t"
			<< "4. Pop back state" << endl
			<< "5. Continue" << endl << endl
			<< ">> ";

		int choice;
		cin >> choice;
		cout << endl;

		switch (choice)
		{
		case 1:

			cout << "You have chosen to add a state to the front of the list" << endl
				 << "Please enter the name of the new state: ";
			cin  >> userInput;
			cout << endl;

			states.push_front(userInput);
			break;

		case 2:

			cout << "You have chosen to add a state to the back of the list" << endl
				 << "Please enter the name of the new state: ";
			cin  >> userInput;
			cout << endl;

			states.push_back(userInput);
			break;

		case 3:
			
			cout << "You have chosen to remove the frontmost state from the list" << endl;
			
			if (states.size() == 0)
			{
				cout << "There are no states to remove!" << endl << endl;
				break;
			}
			else
			{
				cout << "Removed State: " << states.front() << endl << endl;
				states.pop_front();
			}

			
			break;

		case 4:

			cout << "You have chosen to remove the backmost state from the list" << endl;
			
			if (states.size() == 0)
			{
				cout << "There are no states to remove!" << endl << endl;
			}
			else
			{
				cout << "Removed State: " << states.back() << endl << endl;
				states.pop_back();
			}

			break;

		case 5:

			finished = true;
			break;

		default:

			cout << "Invalid input." << endl;
			break;
		}
	}

	cout << "Original: " << endl;
	DisplayList(states);

	cout << "Reversed: " << endl;
	states.reverse();
	DisplayList(states);

	cout << "Sorted: " << endl;
	states.sort();
	DisplayList(states);

	cout << "Reversed Sorted: " << endl;
	states.reverse();
	DisplayList(states);

	cout << endl << "See you later, computer cowboy.";

	cin.ignore();
	cin.get();
	return 0;
}

void  DisplayList(list <string >&  states)

{
	for (list <string >::iterator  it = states.begin();

		it != states.end();

		it++)
	{
		cout << *it << "\t";
	}

	cout << endl;
}
