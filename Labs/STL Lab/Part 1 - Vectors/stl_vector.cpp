// Lab - Standard Template Library - Part 1 - Vectors
// CHASE, STUMP

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{

	vector <string> courses; // string[] courses
	bool done = false;
	int choice;

	while (!done)
	{
		cout << "----------------------------" << endl
			<< "Course count: " << courses.size() << endl << endl
			<< "1. Add a new course\t\t"
			<< "2.Remove the last course" << endl
			<< "3. Display the course list\t"
			<< "4. Quit " << endl << endl;

		
		cin >> choice;

		
		if (choice == 1)
		{
			string courseName; // temp variable
			cout << endl
				 << "You have selected to add a course." << endl
			     << "Please enter desired name: ";
			cin.ignore();
			getline(cin, courseName);
			courses.push_back(courseName);
			cout << endl;
		}
		else if (choice == 2)
		{
			cout << endl
				 << "You have selected to remove the last course." << endl;
			if (courses.size() <= 0)
			{
				cout << "There are no courses to remove." << endl << endl;
			}	
			else
			{
				cout << courses.back() << " was removed" << endl << endl;
				courses.pop_back();
			}
				
			
		}
		else if (choice == 3)
		{
			cout << endl
			     << "You have selected to view the courses." << endl;
			for (unsigned int i = 0; i < courses.size(); i++)
			{
				cout << i + 1 << ". " << courses[i] << endl;
			}
			cout << endl;
		}
		else if (choice == 4)
		{
			cout << "See you later!";
			done = true;
		}
	}
    cin.ignore();
    cin.get();
    return 0;
}
