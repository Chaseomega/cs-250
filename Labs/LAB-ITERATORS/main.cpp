#include <iostream>
#include <fstream>
#include <list>
#include <string>
#include <Windows.h>
using namespace std;

#include "Menu.hpp"

list<string> LoadBook(const string& filename);
void ReadBook(list<string> bookText);

int main()
{
	vector<string> books = { "aesop.txt", "fairytales.txt" };

	bool done = false;
	while (!done)
	{
		Menu::Header("LIBRARY");

		cout << "Which book do you want to read?" << endl;
		int choice = Menu::ShowIntMenuWithPrompt(books);

		list<string> bookText = LoadBook(books[choice - 1]);
		ReadBook(bookText);
	}

	return 0;
}


list<string> LoadBook(const string& filename)
{
	list<string> bookText;

	cout << "Loading " << filename << "..." << endl;

	ifstream input(filename);

	if (!input.good())
	{
		cout << "Error opening file" << endl;
	}

	string line;
	while (getline(input, line))
	{
		bookText.push_back(line);
	}

	cout << endl << bookText.size() << " lines loaded" << endl << endl;

	input.close();

	return bookText;
}


void ReadBook(list<string> bookText)
{
	int totalLines = 0;
	int counter = 0;
	int pageHeight = 25;


	for (list<string>::iterator line = bookText.begin(); line != bookText.end();)
	{
		cout << *line << endl;
		counter++;
		totalLines++;
		line++;

		if (counter == pageHeight)
		{

			Menu::DrawHorizontalBar(80);
			cout << "Line" << totalLines << " / " << bookText.size() << endl;
			int choice = Menu::ShowIntMenuWithPrompt({ "BACKWARD", "FORWARD", "QUIT" }, false);

			if (choice == 1)
			{
				if (totalLines > pageHeight)
				{
					for (int i = 0; i < pageHeight * 2; i++)
					{
						line--;
						totalLines--;
					}
				}
				
				else
				{
					choice = 3;
				}
			}

			if (choice == 3)
			{
				break;
			}

			counter = 0;
		}

	}
}
